//==============================================================================
// HEALMASK - a suite of programs for switching between HEALPix FITS and PNG.
// Copyright 2012 Adam Hincks, Canadian Institute for Theoretical Astrophysics
//
// This file is part of HEALMASK.
//
// HEALMASK is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HEALMASK is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HEALMASK.  If not, see <http://www.gnu.org/licenses/>.
//==============================================================================

#include <chealpix.h>
#include <getopt.h>
#include <malloc.h>
#include <math.h>
#include <png.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

struct option long_opt[] = {
  {"coord-sys", required_argument, NULL, 'c'},
  {"help",      no_argument,       NULL, 'h'},
  {"mask",      no_argument,       NULL, 'm'},
  {"nside",     required_argument, NULL, 'n'},
  {"order-nest", no_argument,      NULL, 'o'},
  {0, 0, 0, 0}
};
char short_opt[] = "c:hmn:o";

void usage();

int main(int argc, char *argv[]) {
  char *path_fits, *path_png, c, *coord_sys;
  unsigned char header[8];
  int i, opt_index, n_pix, is_ring, c_x, c_y, ix, jy, mask;
  int bit_depth, colour_type;
  png_uint_32 n_x, n_y;
  long n_side;
  float *map_out;
  double lat, lon, x, y, res;
  FILE *fp;
  png_structp png_ptr;
  png_infop info_ptr;
  png_bytep *row;

  n_side = 512;
  is_ring = 1;
  mask = 0;
  path_fits = NULL;
  path_png = NULL;
  coord_sys = NULL;
  png_ptr = NULL;
  info_ptr = NULL;
  row = NULL;
  map_out = NULL;
  for (opterr = 0;;) {
    if ((c = getopt_long(argc, argv, short_opt, long_opt, &opt_index)) < 0)
      break;
    switch (c) {
      case 'c':
        if (strcmp(optarg, "C") && strcmp(optarg, "G") && strcmp(optarg, "E"))
          usage();
        coord_sys = strclone(optarg);
        break;
      case 'm':
        mask = 1;
        break;
      case 'n':
        n_side = atoi(optarg);
        for (i = n_side; i >= 2; i /= 2) {
          if (i % 2) {
            fprintf(stderr, "NSIDE must be a power of 2.\n");
            exit(0);
          }
        }
        break;
      case 'o':
        is_ring = 0;
        break;
      case 'h': default:
        usage();
        break;
    }
  }
  if (optind < argc)
    path_png = strclone(argv[optind++]);
  else
    usage();
  if (optind < argc) {
    path_fits = (char *)malloc((strlen(argv[optind]) + 2) * sizeof(char));
    sprintf(path_fits, "%s%s", argv[optind][0] == '!' ? "" : "!", argv[optind]);
    optind++;
  }
  else
    usage();
  if (optind != argc)
    usage();

  // Read in PNG map.
  if ((fp = fopen(path_png, "rb")) == NULL) {
    fprintf(stderr, "Could not open \"%s\" for reading.\n", path_png);
    goto clean_up;
  }

  // Check header.
  i = fread(header, 1, 8, fp);
  if (!png_check_sig(header, 8)) {
    fprintf(stderr, "File \"%s\" is not recognised as a PNG file.\n", path_png);
    goto clean_up;
  }
  
  // Initialise PNG structures.
  if ((png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, 
                                        NULL)) == NULL) {
    fprintf(stderr, "Failure initalising PNG pointer.\n");
    goto clean_up;
  }
  if ((info_ptr = png_create_info_struct(png_ptr)) == NULL) {
    fprintf(stderr, "Failure initalising PNG information pointer.\n");
    goto clean_up;
  }
  if (setjmp(png_jmpbuf(png_ptr))) {
    fprintf(stderr, "Failure initalising PNG callback jumper.\n");
    goto clean_up;
  }
  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, 8);
  png_read_info(png_ptr, info_ptr);

  // Get and check header information.
  png_get_IHDR(png_ptr, info_ptr, &n_x, &n_y, &bit_depth, &colour_type, NULL, 
               NULL, NULL);
  if (colour_type != PNG_COLOR_TYPE_GRAY || bit_depth != 8) {
    fprintf(stderr, "Input PNG must be an 8-bit greyscale image.\n");
    goto clean_up;
  }

  // Prepare to read in bitmap.
  png_read_update_info(png_ptr, info_ptr);
  if (setjmp(png_jmpbuf(png_ptr))) {
    fprintf(stderr, "Error reading PNG image.\n");
    goto clean_up;
  }

  // Read in bitmap and close file.
  row = (png_bytep *)malloc(n_y * sizeof(png_bytep));
  for (i = 0; i < n_y; i++)
    row[i] = (png_bytep)malloc(n_x * sizeof(png_byte));
  png_read_image(png_ptr, row);
  fclose(fp);
  fp = NULL;

  // Figure out resolution and check size.
  project(1.0, 0, 2 * M_PI, &x, &y);
  res = 2 * x / ((double)(n_x) - 0.5);
  project(res, M_PI / 2.0, 0, &x, &y);
  if ((i = (int)y * 2) % 2 == 0)
    i++;
  if (n_y != i) {
    fprintf(stderr, "PNG image dimension is wrong: for width %d, expected "
                    "height %d (not %d).\n", (int)n_x, i, (int)n_y);
    goto clean_up;
  }
  c_x = (int)(n_x / 2.0);
  c_y = (int)(n_y / 2.0);
  n_pix = nside2npix(n_side);

  // Print out some information.
  printf("PNG image of dimension %dx%d read in. Resolution is %g'.\n", 
         (int)n_x, (int)n_y, res * 180.0 * 60.0 / M_PI);

  // Allocate and create the HEALPix map.
  printf("Deprojecting Mollweide map into HEALPix array.\n");
  map_out = (float *)malloc(n_pix * sizeof(float));
  for (i = 0; i < n_pix; i++) {
    if (is_ring)
      pix2ang_ring(n_side, (long)i, &lat, &lon);
    else
      pix2ang_nest(n_side, (long)i, &lat, &lon);
    lat -= M_PI / 2;
    lon -= M_PI;
    project(res, lat, lon, &x, &y);
    ix = (int)(-x) + c_x;   // Note that astronomers like reversing E-W.
    jy = (int)y + c_y;
    if (ix >= n_x || ix < 0 || jy >= n_y || jy < 0)
      printf("%d %d %d\n", i, ix, jy);
    if (mask)
      map_out[i] = row[jy][ix] ? 1.0 : 0.0;
    else
      map_out[i] = (double)row[jy][ix] / (double)0x99;
  }

  // Write HEALPix.
  if (coord_sys == NULL)
    coord_sys = strclone("C");
  printf("NSIDE is %ld, coordinate system '%s' and order is %s.\n", n_side,
         coord_sys, is_ring ? "ring" : "nest");

  // This function has become void for some reason . . .
  //if (write_healpix_map(map_out, n_side, path_fits, !is_ring, coord_sys))
  //  fprintf(stderr, "Error writing HEALPix map to \"%s\".\n", path_fits);
  //else
  //  printf("Map successfully written to \"%s\".\n", path_fits);
  write_healpix_map(map_out, n_side, path_fits, !is_ring, coord_sys);
  printf("Map written to \"%s\".\n", path_fits);

  clean_up:
  free(path_fits);
  free(map_out);
  free(coord_sys);
  if (fp != NULL)
    fclose(fp);
  if (info_ptr != NULL)
    png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
  if (png_ptr != NULL)
    png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
  if (row != NULL) {
    for (i = 0; i < n_y; i++)
      free(row[i]);
  }
  free(row);

  return 0;
}

void usage() {
  printf("png2heal [OPTION]... <PNG file> <FITS file>\n");
  printf("Create a HEALPix FITS file from a PNG image. Use -m to make a "
         "mask.\n");
  printf("\n");
  printf("This program works best if the PNG image was generated by heal2png "
         "and then\n"
         "modified; this will ensure that the dimensions, colour and depth of "
         "the input\n"
         "image are correct. Any existing FITS file will be overwritten.\n");
  printf("\n");
  printf("Options:\n");
  print_usage_opt("-c, --coord-sys=C", "one of 'C', 'G' or 'E'; default is "
                                       "C");
  print_usage_opt("-h, --help", "print this help message and exit");
  print_usage_opt("-m, --mask", "make a map of 0's and 1's, where any non-zero "
                                "input value is assigned a value of 1 in the "
                                "output map; this is useful for making masks");
  print_usage_opt("-n, --nside=NSIDE", "the nside parameter for the HEALPix "
                                       "map (must be a power of 2); default is "
                                       "512");
  print_usage_opt("-o, --order-nest", "do a nested HEALPix ordering; default "
                                      "is ring");
  printf("\n");
  printf("Programmed by Adam Hincks, CITA, 2012.\n");
  exit(0);
}
