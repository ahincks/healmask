//==============================================================================
// HEALMASK - a suite of programs for switching between HEALPix FITS and PNG.
// Copyright 2012 Adam Hincks, Canadian Institute for Theoretical Astrophysics
//
// This file is part of HEALMASK.
//
// HEALMASK is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HEALMASK is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HEALMASK.  If not, see <http://www.gnu.org/licenses/>.
//==============================================================================

#include <math.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define USAGE_LINE_LEN              80
#define PRINT_USAGE_INIT_SPACE       2
#define PRINT_USAGE_COLUMN_SPACE    30

void project(double res, double lat, double lon, double *x, double *y) {
  int i, flag;
  double xpp, ypp, nlon, nlat, cos_lat, sin_lat, cos_cplat, gam, sin_cplat;
  double cos_cplon, sin_cplon, cel_p_lat, cel_p_lon, nat_p_lon, sin_nlat;
  double a, b, c, d, s, fa, fb, fc, fs, z, tol;

  cel_p_lat = M_PI / 2;
  cel_p_lon = 0;
  nat_p_lon = 0;

  // Step 1: rotate to native spherical coordinates.
  cos_lat = cos(lat);
  sin_lat = sin(lat);
  cos_cplat = cos(cel_p_lat);
  sin_cplat = sin(cel_p_lat);
  cos_cplon = cos(lon - cel_p_lon);
  sin_cplon = sin(lon - cel_p_lon);
  nlon = nat_p_lon + atan2(-cos_lat * sin_cplon,
                           sin_lat * cos_cplat -
                           cos_lat * sin_cplat * cos_cplon);
  nlat = asin(sin_lat * sin_cplat + cos_lat * cos_cplat * cos_cplon);

  // Step 2: rotate to projection plane coordinates.
  // First, estimate gamma. It is a transcendental equation: use Brent's method
  // to find the root.
  sin_nlat = sin(nlat);
  a = M_PI;
  fa = sin_nlat - a / (M_PI / 2) - sin(2 * a) / M_PI;
  b = -M_PI;
  fb = sin_nlat - b / (M_PI / 2) - sin(2 * b) / M_PI;
  c = 0;
  fc = 0;
  d = 1e99;
  s = 0;
  fs = 0;
  tol = res / 100.0;
  for (i = 0, flag = 1; fb != 0 && fabs(a - b) > tol; i++) {
    if (i > 1000) {
      fprintf(stderr, "Hmmm, Mollweide projection failed to converge. "
                      "Run away! Run away!\n");
      exit(0);
    }
    if ((fa != fc) && (fb != fc)) {
      // Inverse quadratic interpolation
      s = a * fb * fc / (fa - fb) / (fa - fc) + 
          b * fa * fc / (fb - fa) / (fb - fc) +
          c * fa * fb / (fc - fa) / (fc - fb);
    }
    else {
      // Secant Rule
      s = b - fb * (b - a) / (fb - fa);
    }
 
    z = (3 * a + b) / 4;
    if ((!(((s > z) && (s < b)) || ((s < z) && (s > b)))) || 
        (flag && (fabs(s - b) >= (fabs(b - c) / 2))) ||
        (!flag && (fabs(s - b) >= (fabs(c - d) / 2)))) {
      s = (a + b) / 2;
      flag = 1;
    }
    else {
      if ((flag && (fabs(b - c) < tol)) || 
          (!flag && (fabs(c - d) < tol))) {
        s = (a + b) / 2;
        flag = 1;
      }
      else
        flag = 0;
    }
    
    fs = sin_nlat - s / (M_PI / 2) - sin(2 * s) / M_PI;
    d = c;
    c = b;
    fc = fb;
    if (fa * fs < 0) { 
      b = s; 
      fb = fs; 
    }
    else { 
      a = s; 
      fa = fs; 
    }
    
    if (fabs(fa) < fabs(fb)) { 
      z = a; 
      a = b; 
      b = z; 
      z = fa; 
      fa = fb; 
      fb = z;
    }
  }
  gam = b;

  // Now get the projection plane coordinates.
  xpp = 2 * sqrt(2) / M_PI * nlon * cos(gam);
  ypp = sqrt(2) * sin(gam);

  // Step 3: convert to pixels.
  *x = xpp / res;
  *y = ypp / res;
}

double mod_2pi(double x) {
  long k;
  double y;

  k = (long)(x / M_PI / 2.0);
  y = x - k * M_PI * 2;
  while (y < 0.0)
    y += M_PI * 2;
  while (y >= M_PI * 2)
    y -= M_PI * 2;

  return(y);
}

double mod_360(double x) {
  long k;
  double y;

  k = (long)(x / 360.0);
  y = x - k * 360.0;
  while (y < 0.0)
    y += 360.0;
  while (y >= 360.0)
    y -= 360.0;

  return(y);
}

double mod_pi(double x) {
  double y;

  if ((y = mod_2pi(x)) > M_PI)
    y -= 2 * M_PI;

  return(y);
}

void eq_to_gal(double ra, double dec, double *l, double *b) {
  double cos_b, x1, y1, z1, x2, y2, z2, r;

  cos_b = cos(dec);
  x1 = cos(ra) * cos_b;
  y1 = sin(ra) * cos_b;
  z1 = sin(dec);

  x2 = -0.054875539726 * x1 - 0.873437108010 * y1 - 0.483834985808 * z1;
  y2 =  0.494109453312 * x1 - 0.444829589425 * y1 + 0.746982251810 * z1;
  z2 = -0.867666135858 * x1 - 0.198076386122 * y1 + 0.455983795705 * z1;

  if ((r = sqrt(x2 * x2 + y2 * y2)))
    *l = mod_2pi(atan2(y2, x2));
  else
    *l = 0;
  if (z2)
    *b = mod_2pi(atan2(z2, r));
  else
    *b = 0;

  return;
}

void gal_to_eq(double l, double b, double *ra, double *dec) {
  double cos_b, x1, y1, z1, x2, y2, z2, r;

  cos_b = cos(b);
  x1 = cos(l) * cos_b;
  y1 = sin(l) * cos_b;
  z1 = sin(b);

  x2 = -0.054875539692 * x1 + 0.494109453288 * y1 - 0.867666135842 * z1;
  y2 = -0.873437107998 * x1 - 0.444829589425 * y1 - 0.198076386097 * z1;
  z2 = -0.483834985832 * x1 + 0.746982251827 * y1 + 0.455983795747 * z1;

  if ((r = sqrt(x2 * x2 + y2 * y2)))
    *ra = mod_2pi(atan2(y2, x2));
  else
    *ra = 0;
  if (z2)
    *dec = mod_2pi(atan2(z2, r));
  else
    *dec = 0;

  return;
}

int print_usage_opt(const char *opt, const char *description, ...) {
  char *totstr, *finalstr, *descstr;
  int i, j, k, len;
  va_list argptr;

  descstr = (char *)malloc(strlen(description) * 4);
  totstr = (char *)malloc((strlen(opt) + strlen(description)) * 4);
  finalstr = (char *)malloc((strlen(opt) + strlen(description)) * 4);

  // Insert the option.  
  snprintf(totstr, (strlen(opt) + strlen(description)) * 4, "%*s%s",
           PRINT_USAGE_INIT_SPACE, "", opt);
  for (i = 0, len = PRINT_USAGE_COLUMN_SPACE - strlen(totstr); i < len; i++)
    strcat(totstr, " ");

  // Insert the description.
  va_start(argptr, description);
  vsnprintf(descstr, strlen(description) * 4, description, argptr);
  va_end(argptr);
  strcat(totstr, descstr);

  // Word-wrap and the end of a line.
  strcpy(finalstr, "");
  for (i = 0, j = USAGE_LINE_LEN - 1; j < (int)strlen(totstr);
       j += USAGE_LINE_LEN - PRINT_USAGE_COLUMN_SPACE) {
    for (; j > i && totstr[j] != '\n' && totstr[j] != ' '; j--);
    strncat(finalstr, totstr + i, j - i);
    strcat(finalstr, "\n");
    for (k = 0; k < PRINT_USAGE_COLUMN_SPACE; k++)
      strcat(finalstr, " ");
    i = ++j;
  }
  strncat(finalstr, totstr + i, j - i);

  len = printf("%s\n", finalstr);

  free(descstr);
  free(totstr);
  free(finalstr);

  return len;
}

char *strclone(const char *s) {
  size_t len = strlen(s);
  char *t = (char *)malloc(len + 1);

  strcpy(t, s);

  return t;
}
