//==============================================================================
// HEALMASK - a suite of programs for switching between HEALPix FITS and PNG.
// Copyright 2012 Adam Hincks, Canadian Institute for Theoretical Astrophysics
//
// This file is part of HEALMASK.
//
// HEALMASK is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HEALMASK is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HEALMASK.  If not, see <http://www.gnu.org/licenses/>.
//==============================================================================

#include <chealpix.h>
#include <getopt.h>
#include <malloc.h>
#include <math.h>
#include <png.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

#define DTR 1.7453292519943295769e-2 // Degrees to radians.

struct option long_opt[] = {
  {"coord-sys",  required_argument, NULL, 'c'},
  {"help",       no_argument,       NULL, 'h'},
  {"nside",      required_argument, NULL, 'n'},
  {"order-nest", no_argument,       NULL, 'o'},
  {"rotate",     required_argument, NULL, 'r'},
  {0, 0, 0, 0}
};
char short_opt[] = "c:hn:or:";

void usage();
void do_rotate(int rotate, double *ra, double *dec);

int main(int argc, char *argv[]) {
  char c, *path_coord, *path_fits, *coord_sys, tmpstr[512], type[16];
  int opt_index, i, j, k, n_side, n_pix, n_x, n_y, is_ring, rotate;
  int rotate_back;
  long pix;
  float *map;
  double c_ra, c_dec, c_x, c_y, res_phi, res_theta, x, y, theta, phi, ra, dec;
  double sin_th, cos_th, sin_dec, cos_dec, sin_phi, cos_phi, ra1, ra2, dec1;
  double dec2, eta, lambda, e_min, e_max, l_min, l_max;
  FILE *fp;

  path_coord = NULL;
  path_fits = NULL;
  coord_sys = NULL;
  n_side = 512;
  is_ring = 1;
  rotate = 0;
  rotate_back = 0;
  for (opterr = 0;;) {
    if ((c = getopt_long(argc, argv, short_opt, long_opt, &opt_index)) < 0)
      break;
    switch (c) {
      case 'c':
        if (strcmp(optarg, "C") && strcmp(optarg, "G") && strcmp(optarg, "E"))
          usage();
        coord_sys = strclone(optarg);
        break;
      case 'n':
        n_side = atoi(optarg);
        for (i = n_side; i >= 2; i /= 2) {
          if (i % 2) {
            fprintf(stderr, "NSIDE must be a power of 2.\n");
           exit(0);
          }
        }
        break;
      case 'o':
        is_ring = 0;
        break;
      case 'r':
        if (!strcasecmp(optarg, "E2G")) {
          rotate = E2G;
          rotate_back = G2E;
        }
        else if (!strcasecmp(optarg, "G2E")) {
          rotate = G2E;
          rotate_back = E2G;
        }
        else
          usage();
        break;
      case 'h': default:
        usage();
    }
  }
  if (optind < argc)
    path_coord = strclone(argv[optind++]);
  else
    usage();
  if (optind < argc)
    path_fits = strclone(argv[optind++]);
  else
    usage();
  if (optind != argc)
    usage();

  // Allocate the HEALPix map.
  n_pix = nside2npix(n_side);
  map = (float *)malloc(n_pix * sizeof(float));
  for (i = 0; i < n_pix; i++)
    map[i] = 0;

  // Open the coordinate file.
  if ((fp = fopen(path_coord, "r")) == NULL) {
    fprintf(stderr, "Could not open \"%s\" for reading.\n", path_coord);
    goto clean_up;
  }

  // Fill in the mask based on the coordinates given.
  for (i = 0; fgets(tmpstr, 511, fp) != NULL; i++) {
    if (tmpstr[0] == '#' || tmpstr[0] == '\n')
      continue;
    sscanf(tmpstr, "%15s", type);

    if (!strcasecmp(type, "TAN")) {
      sscanf(tmpstr, "%*s %lf %lf %d %d %lf %lf %lf %lf", &c_ra, &c_dec, &n_x,
             &n_y, &c_x, &c_y, &res_phi, &res_theta);
      printf("Processing TAN.\n");
      res_phi *= DTR;
      res_theta *= DTR;
      c_ra *= DTR;
      c_dec *= DTR;
      sin_dec = sin(c_dec);
      cos_dec = cos(c_dec);

      // Do tangential projection.
      for (j = 0; j < n_x; j++) {
        x = ((double)j - c_x) * res_phi;
        for (k = 0; k < n_y; k++) {
          y = ((double)k - c_y) * res_theta;
          phi = atan2(x, -y);
          theta = atan(1.0 / sqrt(x * x + y * y));
          sin_th = sin(theta);
          cos_th = cos(theta);
          sin_phi = sin(phi - c_ra);
          cos_phi = cos(phi - c_ra);
          ra = c_ra + atan2(-cos_th * sin_phi,
                            sin_th * cos_dec - cos_th * sin_dec * cos_phi);
          dec = asin(sin_th * sin_dec + cos_th * cos_dec * cos(phi));
          
          do_rotate(rotate, &ra, &dec);
          //ra = mod_2pi(ra);
          dec = mod_pi(M_PI / 2.0 - dec);

          if (is_ring)
            ang2pix_ring(n_side, dec, ra, &pix);
          else
            ang2pix_nest(n_side, dec, ra, &pix);

          map[pix] = 1.0;
        }
      }
    }
    else if (!strcasecmp(type, "BOX")) {
      sscanf(tmpstr, "%*s %lf %lf %lf %lf", &ra1, &ra2, &dec1, &dec2);
      printf("Processing BOX.\n");
      ra1 = mod_2pi(ra1 * DTR);
      ra2 = mod_2pi(ra2 * DTR);
      dec1 *= DTR; //mod_pi(M_PI / 2.0 - dec1 * DTR);
      dec2 *= DTR; //mod_pi(M_PI / 2.0 - dec2 * DTR);

      for (pix = 0; pix < n_pix; pix++) {
        if (is_ring)
          pix2ang_ring(n_side, pix, &dec, &ra);
        else
          pix2ang_nest(n_side, pix, &dec, &ra);

        dec = M_PI / 2.0 - dec;
        do_rotate(rotate_back, &ra, &dec);

        if (ra1 < ra2) {
          if (ra >= ra1 && ra <= ra2 && dec >= dec1 && dec <= dec2)
            map[pix] = 1.0;
        }
        else {
          if ((ra >= ra1 || ra <= ra2) && dec >= dec1 && dec <= dec2)
            map[pix] = 1.0;
        }
      }
    }
    else if (!strcasecmp(type, "SDSS")) {
      sscanf(tmpstr, "%*s %lf %lf %lf", &x, &l_min, &l_max);
      printf("Processing SDSS.\n");
      e_min = (x - 2.5) * DTR;
      e_max = (x + 2.5) * DTR;
      l_min *= DTR;
      l_max *= DTR;

      for (pix = 0; pix < n_pix; pix++) {
        if (is_ring)
          pix2ang_ring(n_side, pix, &dec, &ra);
        else
          pix2ang_nest(n_side, pix, &dec, &ra);

        //dec = M_PI / 2.0 - dec;
        do_rotate(rotate_back, &ra, &dec);

        // Now rotate to SDSS survey coordinate system.
        lambda = asin(-cos(ra - 95.0 * DTR) * cos(dec));
        eta = atan2(sin(ra - 95.0 * DTR) * cos(dec), sin(dec)) - 32.5 * DTR;

        if (eta <= e_max && eta >= e_min && lambda <= l_max && lambda >= l_min)
          map[pix] = 1.0;
      }
    }
    else {
      fprintf(stderr, "Could not understand line %d of input file.\n", i);
      goto clean_up;
    }
  }

  // Write HEALPix.
  if (coord_sys == NULL)
    coord_sys = strclone("C");
  printf("NSIDE is %d, coordinate system '%s' and order is %s.\n", n_side,
         coord_sys, is_ring ? "ring" : "nest");
  
  // This function has become void for some reason . . .
  //if (write_healpix_map(map, n_side, path_fits, !is_ring, coord_sys))
  //  fprintf(stderr, "Error writing HEALPix map to \"%s\".\n", path_fits);
  //else
  //  printf("Map successfully written to \"%s\".\n", path_fits);
  write_healpix_map(map, n_side, path_fits, !is_ring, coord_sys);
  printf("Map written to \"%s\".\n", path_fits);

  clean_up:
  fclose(fp);
  free(map);
  free(path_coord);
  free(path_fits);
  free(coord_sys);
}

void do_rotate(int rotate, double *ra, double *dec) {
  double ra2, dec2;

  switch (rotate) {
    case E2G:
      eq_to_gal(*ra, *dec, &ra2, &dec2);
      *ra = ra2;
      *dec = dec2;
      break;
    case G2E:
      gal_to_eq(*ra, *dec, &ra2, &dec2);
      *ra = ra2;
      *dec = dec2;
      break;
  }
  
  return;
}

void usage() {
  printf("coord2heal [OPTION]... <coord file> <FITS file>\n");
  printf("Create a HEALPix FITS mask file from coordinate-defined regions.\n");
  printf("\n");
  printf("Coordinate file format:\n");
  printf("  The file should be an ASCII file. Any line not starting with a "
              "hash ('#')\n"
         "  or a newline is considered a coordinate command. There are XX ways "
              "to define\n"
         "  a region (all units are degrees):\n"
         "  - TAN centre_phi centre_theta n_x n_y centre_x centre_y res_phi "
              "res_theta\n"
         "    o This defines an n_x * n_y region of resolution res_phi and "
              "res_theta, put\n"
         "      onto the sphere with a tangential projection.\n"
         "  - BOX ra_min ra_max dec_min dec_max\n"
         "    o This simply creates a box-shaped mask.\n"
         "  - SDSS eta lambda_min lambda_max\n"
         "    o Makes a box-shaped mask, but in the SDSS survey coordinate "
         "      system.\n");
  printf("\n");
  printf("Options:\n");
  print_usage_opt("-c, --coord-sys=C", "one of 'C', 'G' or 'E'; default is "
                                       "C");
  print_usage_opt("-n, --nside=NSIDE", "the nside parameter for the HEALPix "
                                       "map (must be a power of 2); default is "
                                       "512");
  print_usage_opt("-o, --order-nest", "do a nested HEALPix ordering; default "
                                      "is ring");
  print_usage_opt("-r, --rotate", "rotate between coordinate systems; valid "
                                  "arguments are \"E2G\", \"G2E\" (to go "
                                  "between equatorial and galactic)");
  print_usage_opt("-h, --help", "print this help message and exit");
  printf("\n");
  printf("Programmed by Adam Hincks, CITA, 2012.\n");
  exit(0);
}
