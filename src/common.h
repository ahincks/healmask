//==============================================================================
// HEALMASK - a suite of programs for switching between HEALPix FITS and PNG.
// Copyright 2012 Adam Hincks, Canadian Institute for Theoretical Astrophysics
//
// This file is part of HEALMASK.
//
// HEALMASK is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HEALMASK is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HEALMASK.  If not, see <http://www.gnu.org/licenses/>.
//==============================================================================

#ifndef HEALMASK_COMMON_H
#define HEALMASK_COMMON_H

#include <fitsio.h>

#define S_LINEAR    0
#define S_LOG       1
#define S_SQRT      2
#define S_SQUARE    3

#define E2G         1
#define G2E         2

int print_usage_opt(const char *opt, const char *description, ...);
char *strclone(const char *s);
void project(double res, double lat, double lon, double *x, double *y);
double mod_2pi(double x);
double mod_pi(double x);
double mod_360(double x);
void gal_to_eq(double l, double b, double *ra, double *dec);
void eq_to_gal(double ra, double dec, double *l, double *b);

#endif
