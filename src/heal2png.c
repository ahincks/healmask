//==============================================================================
// HEALMASK - a suite of programs for switching between HEALPix FITS and PNG.
// Copyright 2012 Adam Hincks, Canadian Institute for Theoretical Astrophysics
//
// This file is part of HEALMASK.
//
// HEALMASK is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HEALMASK is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HEALMASK.  If not, see <http://www.gnu.org/licenses/>.
//==============================================================================

#include <chealpix.h>
#include <getopt.h>
#include <malloc.h>
#include <math.h>
#include <png.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

struct option long_opt[] = {
  {"frac-range",    required_argument, NULL, 'f'},
  {"help",          no_argument,       NULL, 'h'},
  {"min-max",       required_argument, NULL, 'm'},
  {"res",           required_argument, NULL, 'r'},
  //{"scale",     required_argument, NULL, 's'},
  {0, 0, 0, 0}
};
char short_opt[] = "f:hm:r:s:";

void write_pixel(png_byte *p, double val);
void usage();
int comp_float(const void *a, const void *b);

int main(int argc, char *argv[]) {
  char *path_fits, *path_png, c, coord_sys, ordering[32], *p;
  int i, j, k, opt_index, scale, n_pix, is_ring, n_x, n_y, c_x, c_y, ix, jy;
  long n_side;
  float *map_in, *map_sort, map_max, map_min, map_range;
  double **map, lat, lon, x, y, res, range_frac;
  FILE *fp;
  png_structp png_ptr;
  png_infop info_ptr;
  png_bytep row;
  png_text title_text;

  path_fits = NULL;
  path_png = NULL;
  res = -1.0;
  scale = S_LINEAR;
  map_max = -5e-10;
  map_min = 5e-10;
  range_frac = 0.98;
  for (opterr = 0;;) {
    if ((c = getopt_long(argc, argv, short_opt, long_opt, &opt_index)) < 0)
      break;
    switch (c) {
      case 'f':
        range_frac = atof(optarg);
        if (range_frac <= 0 || range_frac >= 1.0) {
          fprintf(stderr, "Fractional range must be between 0 and 1.\n");
          exit(0);
        }
        break;
      case 'm':
        if ((p = strchr(optarg, ',')) == NULL)
          usage();
        *p = '\0';
        map_min = atof(optarg);
        map_max = atof(p + 1);
        break;
      case 'r':
        res = atof(optarg) / 60.0 * M_PI / 180.0; // Convert arcmin to radians.
        break;
      /*case 's':
        if (!strcasecmp(optarg, "linear"))
          scale = S_LINEAR;
        else if (!strcasecmp(optarg, "log"))
          scale = S_LOG;
        else if (!strcasecmp(optarg, "sqrt"))
          scale = S_LOG;
        else if (!strcasecmp(optarg, "squared"))
          scale = S_SQUARE;
        else
          usage();
        break;*/
      case 'h': default:
        usage();
        break;
    }
  }
  if (optind < argc)
    path_fits = strclone(argv[optind++]);
  else
    usage();
  if (optind < argc)
    path_png = strclone(argv[optind++]);
  else
    usage();
  if (optind != argc)
    usage();

  // Initialise arrays.
  map = NULL;
  map_in = NULL;
  png_ptr = NULL;
  info_ptr = NULL;
  row = NULL;
  fp = NULL;
  
  // Read in the map.
  if ((map_in = read_healpix_map(path_fits, &n_side, &coord_sys, ordering)) ==
      NULL) {
    fprintf(stderr, "Could not open \"%s\".\n", path_fits);
    goto clean_up;
  }
  printf("---> %d\n", n_side);
  n_pix = nside2npix(n_side);
  printf("Opened map with NSIDE = %ld, coordinate system '%c' and ordering "
         "\"%s\".\n", n_side, coord_sys, ordering);
  if (!strcasecmp(ordering, "RING"))
    is_ring = 1;
  else
    is_ring = 0;

  // Figure out best resolution, if necessary.
  if (res < 0)
    res = sqrt(4 * M_PI / n_pix) * 2.0;

  // Figure out the dynamic range, if necessary.
  if (map_max < -1e-10 && map_min > 1e-10) {
    map_sort = (float *)malloc(n_pix * sizeof(float));
    memcpy(map_sort, map_in, n_pix * sizeof(float));
    if (range_frac < 1.0)
      qsort(map_sort, (size_t)n_pix, (size_t)sizeof(float), comp_float);
    j = (int)((double)n_pix * (1.0 - range_frac) / 2.0);
    k = n_pix - j;
    for (i = j; i < k; i++) {
      if (map_sort[i] > map_max)
        map_max = map_sort[i];
      if (map_sort[i] < map_min)
        map_min = map_sort[i];
    }
    free(map_sort);
  }
  printf("Dynamic range is %g to %g.\n", map_min, map_max);
  map_range = map_max - map_min;

  // Figure out the size of the bitmap, allocate and initialise.
  project(res, 0, M_PI * 2, &x, &y);
  n_x = 2 * (int)x + 1;
  c_x = (int)x;
  project(res, M_PI / 2, 0, &x, &y);
  n_y = 2 * (int)y + 1;
  c_y = (int)y;
  map = (double **)malloc(n_x * sizeof(double *));
  for (i = 0; i < n_x; i++) {
    map[i] = (double *)malloc(n_y * sizeof(double));
    for (j = 0; j < n_y; j++)
      map[i][j] = 0.0;
  }
  printf("Resolution is %.2f' and map size is %dx%d\n", 
         res * 180.0 * 60.0 / M_PI, n_x, n_y);

  // Create the bitmap.
  printf("Performing Mollweide's projection.\n");
  for (i = 0; i < n_pix; i++) {
    if (is_ring)
      pix2ang_ring(n_side, (long)i, &lat, &lon);
    else
      pix2ang_nest(n_side, (long)i, &lat, &lon);

    lat -= M_PI / 2;
    lon -= M_PI;
    project(res, lat, lon, &x, &y);
    ix = (int)(-x) + c_x;   // Note that astronomers like reversing E-W.
    jy = (int)y + c_y;
    map[ix][jy] = (double)((map_in[i] - map_min) / map_range);
  }

  // Open file for writing png.
  if ((fp = fopen(path_png, "wb")) == NULL) {
    fprintf(stderr, "Could not open file \"%s\" for writing.\n", path_png);
    goto clean_up;
  }

  // Initialize write structure.
  if ((png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL,
                                         NULL)) == NULL) {
    fprintf(stderr, "Could not allocate PNG write struct.\n");
    goto clean_up;
  }

  // Initialize info structure
  info_ptr = png_create_info_struct(png_ptr);
  if (info_ptr == NULL) {
    fprintf(stderr, "Could not allocate PNG info struct.\n");
    goto clean_up;
  }

  // Setup exception handling.
  if (setjmp(png_jmpbuf(png_ptr))) {
    fprintf(stderr, "Error during png creation.\n");
    goto clean_up;
  }

  png_init_io(png_ptr, fp);

  // Write header.
  png_set_IHDR(png_ptr, info_ptr, n_x, n_y, 8, PNG_COLOR_TYPE_GRAY,
               PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE,
               PNG_FILTER_TYPE_BASE);

  // Set title
  if ("title" != NULL) {
    title_text.compression = PNG_TEXT_COMPRESSION_NONE;
    title_text.key = "Title";
    title_text.text = "PNG created from HEALPix map.";
    png_set_text(png_ptr, info_ptr, &title_text, 1);
  }

  png_write_info(png_ptr, info_ptr);

  // Allocate memory for one row (3 bytes per RGB pixel).
  row = (png_bytep)malloc(n_x * sizeof(png_byte));
  for (i = 0; i < n_x; i++)
    row[i] = 0;

  // Write image data.
  for (j = 0; j < n_y; j++) {
    for (i = 0; i < n_x; i++)
      write_pixel(&row[i], map[i][j]);
    png_write_row(png_ptr, row);
  }

  // End write
  png_write_end(png_ptr, NULL);
  printf("Successfully wrote image to \"%s\".\n", path_png);

  clean_up:
  free(path_fits);
  free(map);
  if (fp != NULL)
    fclose(fp);
  if (info_ptr != NULL)
    png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
  if (png_ptr != NULL)
    png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
  if (row != NULL)
    free(row);

  return 0;
}

int comp_float(const void *a, const void *b) {
  if (*((float *)a) == *((float *)b))
    return 0;
  else if (*((float *)a) < *((float *)b))
    return -1;
  else
    return 1;
}

void write_pixel(png_byte *p, double val) {
  if (val > 1.0)
    val = 1.0;
  if (val < 0)
    val = 0;
  p[0] = val * 0x99;

  return;
}

void usage() {
  printf("heal2png [OPTION]... <FITS file> <PNG file>\n");
  printf("Create a PNG image from a HEALPix FITS file.\n");
  printf("\n");
  printf("Options:\n");
  print_usage_opt("-f, --frac-range=F", "for the dynamic range, use values "
                                        "that fall within F around the median "
                                        "value; i.e., F=0.98 excludes the top "
                                        "and bottom 2%% of pixels; default is "
                                        "0.98");
  print_usage_opt("-h, --help", "print this help message and exit");
  print_usage_opt("-m, --min-max=MIN,MAX", "specify the minimum and maximum "
                                           "pixel values to use for the "
                                           "dynamic range; overwrites "
                                           "--frac-range");
  print_usage_opt("-r, --res=RES", "the resolution for the PNG image, in "
                                   "arcminutes; by default, heal2png chooses a "
                                   "resolution that gives roughly a couple of "
                                   "PNG pixel per HEALpix pixel");
  //print_usage_opt("-s, --scale=TYPE", "one of \"linear\", \"log\", \"sqrt\" or "
  //                                    "\"squared\"");
  printf("\n");
  printf("Programmed by Adam Hincks, CITA, 2012.\n");
  exit(0);
}
